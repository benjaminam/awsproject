import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FileUploaderService } from 'src/app/file-uploader.service';
import { People } from 'src/app/interfaces/people';

@Component({
  selector: 'app-upload-data',
  templateUrl: './upload-data.component.html',
  styleUrls: ['./upload-data.component.css']
})
export class UploadDataComponent implements OnInit {
  ngOnInit(): void {
  }
  fileObj: File;
  fileUrl: string;
  errorMsg: boolean;
  title = 'awsproject';
  people: People[];
  constructor(private fileUploaderService: FileUploaderService) {
    this.errorMsg = false;
  }
  onFilePicked(event: Event): void {
    this.errorMsg = false;
    console.log(event);
    const FILE = (event.target as HTMLInputElement).files[0];
    this.fileObj = FILE;
    console.log(this.fileObj);
  }

  onUpload() {
    console.log("onUpload()", this.fileObj);
    if (!this.fileObj) {
      this.errorMsg = true;
      return
    }
    const fileForm = new FormData();
    fileForm.append('file', this.fileObj);
     ( this.fileUploaderService.fileUpload(fileForm)).subscribe(res => {

           {
         res.similarity ?
           res.similarity = parseFloat(res.similarity).toFixed(2)
           :
           null
       }
       this.people = res;
       console.log(this.people)
       console.log(res.similarity)
     });
   }
   
  refresh() {
    this.people=null;
   }
}
