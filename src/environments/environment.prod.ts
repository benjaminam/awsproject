export const environment = {
  production: true,
  sso_api_username: '',
  sso_api_pwd: '',

  loginURL: 'https://projecturi.auth.us-east-1.amazoncognito.com/login?' +
              'client_id=6o66c5ratuefhe5hme3rcpu9el&response_type=code&scope=openid+profile&' +
              'redirect_uri=http://localhost:4200/upload1',

  redirectURL: 'https://projectcloudben.s3-website-us-east-1.amazonaws.com/upload1',

  cognitoTokenURL: 'https://projecturi.auth.us-east-1.amazoncognito.com/oauth2/token',

  logout: 'https://projecturi.auth.us-east-1.amazoncognito.com/logout?' +
          'client_id=6o66c5ratuefhe5hme3rcpu9el&' +
          'logout_uri=https://projectcloudben.s3-website-us-east-1.amazonaws.com/home'



};
