// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  sso_api_username: '',
  sso_api_pwd: '',

  loginURL: 'https://projecturi.auth.us-east-1.amazoncognito.com/login?' +
              'client_id=6o66c5ratuefhe5hme3rcpu9el&response_type=code&scope=openid+profile&' +
              'redirect_uri=http://localhost:4200/upload1',

  redirectURL: 'http://localhost:4200/upload1',

  cognitoTokenURL: 'https://projecturi.auth.us-east-1.amazoncognito.com/oauth2/token',

  logout: 'https://projecturi.auth.us-east-1.amazoncognito.com/logout?' +
          'client_id=6o66c5ratuefhe5hme3rcpu9el&' +
          'logout_uri=http://localhost:4200/home'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
